
import java.util.Scanner;
import java.util.Arrays;

class AllArrays1 {
		
	public static int arraySum (int array []) {
		int arraySum=0;

		for (int i=0; i < array.length; i++) {
			arraySum = arraySum+array[i];
		}
		return arraySum;
	}	

	public static void revers (int array []) {

		int temp; 
		for (int i=0; i< array.length/2; i++) {
			temp = array[array.length-i-1];
			array[array.length-i-1] = array[i];
			array[i] = temp;
		}
	}		

	public static int average (int array []) {	
		
		int arrayAverage = 0;
		int arraySum=0;
		for (int i = array.length-1;  i >= 0; i--) {
			
			arraySum = arraySum+array[i];
			arrayAverage = arraySum/array.length;
		}

		return arrayAverage;
	}

	public static void minAndMAx (int array []) {	
			
		int min = array[0];
		int positionOFMin=0;
		int max = array[0];
		int positionOFMax=0;
		int temp; 
		for (int i=0; i< array.length; i++) {
			if (array[i] < min) {
				min = array[i];
				positionOFMin = i;
			}	
			if (array[i] > max) {
				max = array[i]; 
				positionOFMax = i;
			} 
		}	
		temp = array[positionOFMax];
		array[positionOFMax] = array[positionOFMin];
		array[positionOFMin] = temp;
	}    		

	public static void bubble (int array []) {		
		boolean sort = false;
		while (!sort) {
			sort = true;
			for (int i = 1; i < array.length; i++) {
				if (array[i] < array[i-1]) {
					int temp = array[i];
					array[i] = array[i-1];
					array[i-1] = temp;
					sort = false;
			    }
			}	
		}
	}	

	public static int arrayToNumber (int array []) {		
		int number = 0;
		int degree = 1;
		
			for (int i = array.length-1; i >=0; i--) {
				number= number + array[i]*degree;
				degree = degree*10;	
			}
		return number;	
	}

	public static void main(String[] args) {
		System.out.println("Enter numbers: ");
		Scanner scanner = new Scanner(System.in);
		int size = scanner.nextInt();
		int array[] = new int[size];

		for (int i = 0; i < array.length; i++) {
			array[i] = scanner.nextInt();
			
		}
		System.out.println(Arrays.toString(array));

		arraySum (array);
		arrayToNumber(array);
		average(array);
		System.out.println("Sum of array:  " + arraySum(array));
		System.out.println("Array is Number:  " + arrayToNumber(array));
		System.out.println("Array average is:  " + average(array));
		revers(array);
		System.out.println("Array is revers:  " +Arrays.toString(array));
		minAndMAx (array);
		System.out.println("Array change min and max:  " + Arrays.toString(array));
		bubble (array);
		System.out.println("Bubble sort:  " + Arrays.toString(array));
	}	
}