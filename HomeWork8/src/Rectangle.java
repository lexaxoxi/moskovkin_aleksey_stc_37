public class Rectangle extends Figure {

    public Rectangle(double a, double b) {
        super(a, b,0,0);
    }
    @Override
    public double perimetr() {
        return (a + b) * 2;
    }

    @Override
    public double square() {

        return (a * b);

   }
}
