public class Circle extends Figure {

    public Circle(double a, double b) {
        super(a,b,0,0);
    }


    @Override
    public double perimetr() {

        return 2*PI*a;
    }

    @Override
    public double square() {
        return PI*a*a;
    }

}
