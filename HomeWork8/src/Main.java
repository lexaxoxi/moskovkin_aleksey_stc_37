public class Main {
    public static void main(String[] args) {

        Rectangle rectangle = new Rectangle(8, 5);
        rectangle.perimetr();
        rectangle.square();
        System.out.println("периметр прямоугольника:   " + rectangle.perimetr() + ";" + "  площадь прямоугольника:   " + rectangle.square());

        Circle circle = new Circle(3, 4);
        circle.perimetr();
        circle.square();
        System.out.println("длина окружности:   " + circle.perimetr() + ";" + "  площадь прямоугольника:   " + circle.square());

        Quadrate quadrate = new Quadrate(4, 5);
        quadrate.perimetr();
        quadrate.square();
        System.out.println("периметр квадрата:   " + quadrate.perimetr() + ";" + "  площадь квадрата:   " + quadrate.square());


        Ellipse ellipse = new Ellipse(3, 2);
        ellipse.perimetr();
        ellipse.square();
        System.out.println("периметр эллипса:   " + ellipse.perimetr() + ";" + "  площадь эллипса:   " + ellipse.square());
        ellipse.size(3);
        ellipse.moveTo(3, 20);
    }
}
