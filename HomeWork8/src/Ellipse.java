public class Ellipse extends Figure {
    public Ellipse(double a, double b) {
        super(a, b,0,0);
    }

    @Override
    public double perimetr() {
        return PI * (a + b);
    }

    @Override
    public double square() {
        return PI * a * b;

    }
}
