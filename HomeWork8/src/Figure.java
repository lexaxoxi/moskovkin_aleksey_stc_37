
public abstract class Figure implements Scaling, Moving {
    public static final double PI = 3.14;
    double a;
    double b;
    double perimetr;
    double square;
    int x;
    int y;

    public Figure(double a, double b, int x, int y) {

        if (a > 0) {
            this.a = a;

        }
        if (b > 0) {
            this.b = b;
        }
        this.x = x;
        this.y = y;
    }

    public double getA() {
        return a;
    }

    public double getB() {
        return b;
    }


    @Override
    public void moveTo(int dx, int dy) {
        x += dx;
        y += dy;
        System.out.println("Центр фигуры переместился на следующие координаты:   " + "x: " + dx +"; " + "y: " + dy +".");
    }


    @Override
    public void size(double coefficient) {
        a = a * coefficient;
        b = b * coefficient;
        System.out.println("Новое значение стороны A или радиуса:     " + a + ";" + "  новое значение стороны B: " + b);
    }

    public abstract double perimetr();

    public abstract double square();

}

