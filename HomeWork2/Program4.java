
import java.util.Scanner;
import java.util.Arrays;

class Program4 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int n = scanner.nextInt();
		int array [] = new int[n];
		
		for (int i = array.length-1;  i >= 0; i--) {
			array[i] = scanner.nextInt();
		}
	    int min = array[0];
		int positionOFMin=0;
		int max = array[0];
		int positionOFMax=0;
		int temp; 


		for (int i=0; i< array.length; i++) {
			if (array[i] < min) {
				min = array[i];
				positionOFMin = i;
			}	
			if (array[i] > max) {
				max = array[i]; 
				positionOFMax = i;
			} 
		}
		System.out.println("array: " + Arrays.toString(array));
		System.out.println("min number " + min);
		System.out.println("max number " + max);

		temp = array[positionOFMax];
		array[positionOFMax] = array[positionOFMin];
		array[positionOFMin] = temp;

		System.out.println("new array: " + Arrays.toString(array));
	}
}