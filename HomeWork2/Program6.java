import java.util.Arrays;

class Program6 {
	public static void main(String[] args) {
		int array [] = {4,2,3,5,7};
		int number = 0;
		int degree = 1;
		System.out.println("array: " + Arrays.toString(array));	

		for (int i = array.length-1; i >=0; i--) {
				number= number + array[i]*degree;
				degree = degree*10;
		}		
		System.out.println("Convert array to number: " + number);
	}
}