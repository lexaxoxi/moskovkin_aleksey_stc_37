import java.util.Scanner;
import java.util.Arrays;

class Bubble {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int size = scanner.nextInt();
		int array [] = new int[size];
		for (int i = 0; i < array.length; i++) {
			        array[i] = scanner.nextInt();
		}	
	System.out.println("array: " + Arrays.toString(array));	

	    boolean sort = false;
		while (!sort) {
			sort = true;
			for (int i = 1; i < array.length; i++) {
				if (array[i] < array[i-1]) {
					int temp = array[i];
					array[i] = array[i-1];
					array[i-1] = temp;
					sort = false;
			    }
			}	
		}
     System.out.println("new array : " + Arrays.toString(array));
	}
}			  