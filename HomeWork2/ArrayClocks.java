//ДЗ 2 задание 7

class ArrayClocks {
 
    public static void main(String[] args) {
        
        int a = 3;
        int b = 3;
        int s = 1;
        int[][] array = new int[a][b];
       
        for (int y = 0; y < b; y++) {
            array[0][y] = s;
            s++;
        }
        for (int x = 1; x < a; x++) {
            array[x][b - 1] = s;
            s++;
        }
        for (int y = b - 2; y >= 0; y--) {
            array[a - 1][y] = s;
            s++;
        }
        for (int x = a - 2; x > 0; x--) {
            array[x][0] = s;
            s++;
        }
        int c = 1;
        int d = 1;
 
        while (s < a * b) {
            while (array[c][d + 1] == 0) {
                array[c][d] = s;
                s++;
                d++;
            }
            while (array[c + 1][d] == 0) {
                array[c][d] = s;
                s++;
                c++;
            }
            while (array[c][d - 1] == 0) {
                array[c][d] = s;
                s++;
                d--;
            }
            while (array[c - 1][d] == 0) {
                array[c][d] = s;
                s++;
                c--;
            }
        }
        for (int x = 0; x < a; x++) {
            for (int y = 0; y < b; y++) {
                if (array[x][y] == 0) {
                    array[x][y] = s;
                }
            }
        }
        for (int x = 0; x < a; x++) {
            for (int y = 0; y < b; y++) {
                if (array[x][y] < 10) {
                    System.out.print(array[x][y] + ",  ");
                } else {
                    System.out.print(array[x][y] + ", ");
                }
            }
            System.out.println("");
        }
    }
}