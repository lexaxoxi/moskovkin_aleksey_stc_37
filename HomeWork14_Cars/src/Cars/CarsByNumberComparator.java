package Cars;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

class CarsByNumberComparator implements CarRepository {
    private static final String PATH_TO_AUTO = "DB of Cars.txt";


    private final Function<String, Car> carMapper = line -> {
        if (line == null || line.isEmpty()) {
            return null;
        } else {
            line = line.substring(1, line.length() - 1);
            String[] parsedLine = line.split("]\\[");
            return new Car(parsedLine[0],
                    parsedLine[1],
                    parsedLine[2],
                    Long.parseLong (parsedLine[3]),
                    Long.parseLong (parsedLine[4]));

        }
    };
    private <T> T common(String fileName, Function<BufferedReader, T> function) {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            return function.apply(reader);
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public List<Car> findAll() {
        return common(PATH_TO_AUTO, reader ->
                reader.lines()
                        .map(carMapper)
                        .sorted(Comparator.comparing(Car::getModelOfCar))
                        .collect(Collectors.toList()));
    }

    @Override
    public List<String> findBlackCarsAndZeroMileage() {
        return common(PATH_TO_AUTO, reader ->
                reader.lines()
                        .map(carMapper)
                        .filter(car -> car.getColorOfCar().equals("black") || car.getMileageOfCar() == 0)
                        .map(Car::getNumberOfCar)
                        .collect(Collectors.toList()));
    }

    @Override
    public Long findAmountModelsOfCarsWithPriceBetween700And800() {
        return common(PATH_TO_AUTO, reader ->
                reader.lines()
                        .map(carMapper)
                        .filter(car -> car.getPriceOfCar() >= 700000 && car.getPriceOfCar() <= 800000)
                        .map(Car::getModelOfCar)
                        .distinct()
                        .count());
    }

    @Override
    public String findColorOfCarWithMinPrice() {
        return common(PATH_TO_AUTO, reader ->
                reader.lines()
                        .map(carMapper)
                        .min(Comparator.comparing(Car::getPriceOfCar))
                        .map(Car::getColorOfCar)
                        .orElse("Нам кажется, что список пуст"));
    }

    @Override
    public Double findAveragePriceOfCamry() {
        return common(PATH_TO_AUTO, reader ->
                reader.lines()
                        .map(carMapper)
                        .filter(car -> car.getModelOfCar().equals("Camry"))
                        .mapToLong(Car::getPriceOfCar)
                        .average()
                        .orElse(0.0));
    }
}


