package Cars;

public class Main {

    public static void main(String[] args) {
        CarRepository carRepository = new CarsByNumberComparator ();
        System.out.println("Контрольный перечень автомобилей:");
        System.out.println(carRepository.findAll());
        System.out.println();
        System.out.println("Номера черных автомобилей и/или с нулевым пробегом:");
        System.out.println(carRepository.findBlackCarsAndZeroMileage());
        System.out.println();
        System.out.println("Количество уникальных моделей в ценовом диапазоне от 700000 до 800000:");
        System.out.println(carRepository.findAmountModelsOfCarsWithPriceBetween700And800());
        System.out.println();
        System.out.println("Автомобиль с минимальной ценой имеет следующий цвет:");
        System.out.println(carRepository.findColorOfCarWithMinPrice());
        System.out.println();
        System.out.println("Средняя стоимость автомобилей Camry");
        System.out.println(carRepository.findAveragePriceOfCamry());
    }
}

//была проблема с размещением файла txt