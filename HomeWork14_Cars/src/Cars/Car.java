package Cars;

import java.util.Objects;

public class Car {
    private String NumberOfCar;
    private Long MileageOfCar;
    private Long PriceOfCar;
    private String ModelOfCar;
    private String ColorOfCar;


    public Car(String NumberOfCar, String ModelOfCar, String ColorOfCar, Long MileageOfCar, Long PriceOfCar) {
        this.NumberOfCar = NumberOfCar;
        this.ModelOfCar = ModelOfCar;
        this.ColorOfCar = ColorOfCar;
        this.MileageOfCar = MileageOfCar;
        this.PriceOfCar = PriceOfCar;
    }

    public String getNumberOfCar() {
        return NumberOfCar;
    }

    public void setNumberOfCar(String numberOfCar) {
        this.NumberOfCar = numberOfCar;
    }

    public String getModelOfCar() {
        return ModelOfCar;
    }

    public void setModelOfCar(String modelOfCar) {
        this.ModelOfCar = modelOfCar;
    }

    public String getColorOfCar() {
        return ColorOfCar;
    }

    public void setColorOfCar(String colorOfCar) {
        this.ColorOfCar = colorOfCar;
    }

    public long getMileageOfCar() {
        return MileageOfCar;
    }

    public void setMileageOfCar(long mileageOfCar) {
        this.MileageOfCar = mileageOfCar;
    }

    public long getPriceOfCar() {
        return PriceOfCar;
    }

    public void setPriceOfCar(long PriceOfCar) {
        this.PriceOfCar = PriceOfCar;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return MileageOfCar == car.MileageOfCar &&
                PriceOfCar == car.PriceOfCar &&
                Objects.equals(NumberOfCar, car.NumberOfCar) &&
                Objects.equals(ModelOfCar, car.ModelOfCar) &&
                Objects.equals(ColorOfCar, car.ColorOfCar);
    }

    @Override
    public int hashCode() {
        return Objects.hash(NumberOfCar, ModelOfCar, ColorOfCar, MileageOfCar, PriceOfCar);
    }

    @Override
    public String toString() {
        return "number: " + NumberOfCar
                + " /model: '" + ModelOfCar + "'"
                + " color:  '" + ColorOfCar + "'"
                + "    mileage: " + MileageOfCar
                + "    price: " + PriceOfCar + "\n \n";
    }
}

