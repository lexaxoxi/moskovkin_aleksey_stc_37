import java.util.Scanner;

class Program3Primes {
	public static void main(String[] args) {
		System.out.println("Enter numbers: ");
		Scanner scanner = new Scanner(System.in);
		int number = scanner.nextInt();
		long multiplication = 1;
        int tempNumber;

		while(number != 0) {  
			int digitsSum = 0; 
			int tempSum=number;
			boolean prime = true;

			 while(tempSum != 0) {    
			 	digitsSum = digitsSum + tempSum%10; 
			 	tempSum = tempSum/10;
			}
			for (int i = 2; i < digitsSum; i++) {  
   			    tempNumber = digitsSum %i;
   				if (tempNumber ==0) {
					prime = false;
	  			  	break;
   				}	
			}
				if (prime == true) {
				multiplication=number*multiplication;
				}	
			number = scanner.nextInt();  
		} 
		System.out.println("multiplication:  " + multiplication);	
	}
}
