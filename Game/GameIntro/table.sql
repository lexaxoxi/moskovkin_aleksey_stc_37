create table Player
(
    id         bigserial primary key,
    ipAddress  varchar(20),
    name       varchar(20),
    points     integer,
    countWins  integer,
    countLoses integer
);

create table Game
(
    id                     bigserial primary key,
    dateTime               timestamp,
    playerFirst_id         bigint,
    playerSecond_id        bigint,
    countShotsPlayerFirst  integer,
    countShotsPlayerSecond integer,
    secondsGameTimeAmount  bigint,
    foreign key (playerFirst_id) references Player (id),
    foreign key (playerSecond_id) references Player (id)
);



create table Shot
(
    id         bigserial primary key,
    shotTime   timestamp,
    game_id    bigint,
    shooter_id bigint,
    target_id  bigint,
    foreign key (game_id) references Game (id),
    foreign key (shooter_id) references Player (id),
    foreign key (target_id) references Player (id)
);