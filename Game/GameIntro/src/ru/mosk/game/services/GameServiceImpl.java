package ru.mosk.game.services;

import ru.mosk.game.dto.StatisticDto;
import ru.mosk.game.models.Game;
import ru.mosk.game.models.Player;
import ru.mosk.game.models.Shot;
import ru.mosk.game.repository.GamesRepository;
import ru.mosk.game.repository.PlayersRepository;
import ru.mosk.game.repository.ShotsRepository;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class GameServiceImpl implements GameService {
    private PlayersRepository playersRepository;
    private GamesRepository gamesRepository;
    private ShotsRepository shotsRepository;

    public GameServiceImpl(PlayersRepository playersRepository, GamesRepository gamesRepository, ShotsRepository shotsRepository) {
        this.playersRepository = playersRepository;
        this.gamesRepository = gamesRepository;
        this.shotsRepository = shotsRepository;
    }

    @Override
    public Long startGame(String firstIp, String secondIp, String firstPlayerNickname, String secondPlayerNickname) {

        //получили инф-цию об обоих игроках
        Player first = checkIfExists(firstIp, firstPlayerNickname);
        Player second = checkIfExists(secondIp, secondPlayerNickname);
        //создали игру
        // todo: долго боролся, просил конструктор
        //должны ли быть id в объявлении новой игры?? По сути нет. И в shot Также
        Game game = new Game (LocalDateTime.now(), first, second, 0, 0, 0l);
        // сохранили игру в репозитории
        gamesRepository.save(game);
        return game.getId();
    }

    private Player checkIfExists(String ip, String nickname) {
        Player player = playersRepository.findByNickname(nickname);
        // если нет игрока
        if (player == null) {
            //создаем игарока
            player = new Player(ip, nickname, 0, 0, 0);
            //сохраняем его в репозитории
            playersRepository.save(player);
        } else {
            // если такой игрок сузествует - обновляем у него IP адрес
            player.setIp(ip);
            playersRepository.update(player);
        }
        return player;
    }

    @Override
    public void shot(Long gameId, String shooterNickname, String targetNickname) {
        Player shooter = playersRepository.findByNickname(shooterNickname);
        Player target = playersRepository.findByNickname(targetNickname);
        Game game = gamesRepository.findById(gameId);
        Shot shot = new Shot(LocalDateTime.now(), game, shooter, target);

        shooter.setPoints(shooter.getPoints() + 1);

        if (game.getPlayerFirst().getName().equals(shooterNickname)) {
            game.setPlayerFirstShotsCount(game.getPlayerFirstShotsCount() + 1);
        }

        if (game.getPlayerSecond().getName().equals(shooterNickname)) {
            game.setPlayerSecondShotsCount(game.getPlayerSecondShotsCount() + 1);
        }

        playersRepository.update(shooter);
        gamesRepository.update(game);
        shotsRepository.save(shot);
    }

    @Override
    public StatisticDto finishGame(Long gameId) {
        //получили игру из репозитория
        Game game = gamesRepository.findById(gameId);
        StatisticDto statisticDto = new StatisticDto(game);
        if (game.getPlayerFirstShotsCount() > game.getPlayerSecondShotsCount()) {
            game.getPlayerFirst().setMaxWinsCount(game.getPlayerFirst().getMaxWinsCount() + 1);
            game.getPlayerSecond().setMaxLosesCount(game.getPlayerSecond().getMaxLosesCount() + 1);
        } else if (game.getPlayerFirstShotsCount() < game.getPlayerSecondShotsCount()) {
            game.getPlayerFirst().setMaxLosesCount(game.getPlayerFirst().getMaxLosesCount() + 1);
            game.getPlayerSecond().setMaxWinsCount(game.getPlayerSecond().getMaxWinsCount() + 1);
        }

        playersRepository.update(game.getPlayerFirst());
        playersRepository.update(game.getPlayerSecond());
        long time = Duration.between(LocalTime.now(), game.getDateTime()).getSeconds();
        game.setSecondsGameTimeAmount(Math.abs(time));
        gamesRepository.update(game);
        return statisticDto;
    }
}
