package ru.mosk.game.repository;

import ru.mosk.game.models.Game;

import java.util.ArrayList;
import java.util.List;

public class GamesRepositoryImpl implements GamesRepository {
    private List<Game> games;

    public GamesRepositoryImpl () {
        games = new ArrayList<>();
    }

//    public GamesRepositoryImpl(List<Game> games) {
//        this.games = games;
//    }

    @Override
    public void save(Game game) {
        game.setId((long) games.size());
        games.add(game);
    }

    @Override
    public Game findById(Long gameId) {
        return games.get(gameId.intValue());
    }

    @Override
    public void update(Game game) {
        games.set(game.getId().intValue(), game);
    }
}
