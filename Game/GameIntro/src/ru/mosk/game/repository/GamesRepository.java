package ru.mosk.game.repository;

import ru.mosk.game.models.Game;

public interface GamesRepository {
    void save (Game game);

    Game findById (Long gameId);

    void  update (Game game);

}
