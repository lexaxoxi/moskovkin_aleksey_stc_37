package ru.mosk.game.repository;

import ru.mosk.game.models.Shot;

import java.io.*;

public class ShotsRepositoryFileImpl implements ShotsRepository {
    private String dbfileName;
    private String sequenceFileName;

    public ShotsRepositoryFileImpl(String dbfileName, String sequenceFileName) {
        this.dbfileName = dbfileName;
        this.sequenceFileName = sequenceFileName;
    }

    @Override
    public void save(Shot shot) {
        try {
           BufferedWriter writer = new BufferedWriter((new FileWriter(dbfileName, true)));
           shot.setId(generateId());

            writer.write(shot.getId() + " # " + shot.getDateTime().toString() + "#" +
                    shot.getGame().getId() + "#" + shot.getShooter().getName() + "#" +
                    shot.getTarget().getName() + "\n");
            writer.close();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }

    }

    private Long generateId() {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(sequenceFileName));
            //прочитали последний сгенеррированный id как строку
            String lastGeneratedIdAsString = reader.readLine();
            //преобразовываем ее в Long
            long id = Long.parseLong(lastGeneratedIdAsString);
            reader.close();

            BufferedWriter writer = new BufferedWriter( (new FileWriter(sequenceFileName)));
            writer.write(String.valueOf(id+1));
            writer.close();

            return id;
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }

    }
}
