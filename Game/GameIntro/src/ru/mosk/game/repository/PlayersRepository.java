package ru.mosk.game.repository;

import ru.mosk.game.models.Player;

public interface PlayersRepository {
    Player findByNickname (String nickname);

    void save (Player player);

    void update(Player player);
}
