package  ru.mosk.game.models;;

import java.util.Objects;
import java.util.StringJoiner;

public class Player {
    private Long id;
    private String ip;
    private String name;
    private Integer points;
    private Integer maxWinsCount ;
    private Integer maxLosesCount;

    public Player(String ip, String name, Integer points, Integer maxWinsCount, Integer maxLosesCount) {
        this.ip = ip;
        this.name = name;
        this.points = points;
        this.maxWinsCount = maxWinsCount;
        this.maxLosesCount = maxLosesCount;
    }

    public Player(long playerfirst, String playerfirst_ip, String playerfirst_name, int playerfirst_points, int playerfirst_maxWinsCount, int playerfirst_maxLosesCount) {
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public Integer getMaxWinsCount() {
        return maxWinsCount;
    }

    public void setMaxWinsCount(Integer maxWinsCount) {
        this.maxWinsCount = maxWinsCount;
    }

    public Integer getMaxLosesCount() {
        return maxLosesCount;
    }

    public void setMaxLosesCount(Integer maxLosesCount) {
        this.maxLosesCount = maxLosesCount;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Player player = (Player) o;
        return Objects.equals(ip, player.ip) && Objects.equals(name, player.name) && Objects.equals(points, player.points) && Objects.equals(maxWinsCount, player.maxWinsCount) && Objects.equals(maxLosesCount, player.maxLosesCount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ip, name, points, maxWinsCount, maxLosesCount);
    }
    @Override
    public String toString() {
        return new StringJoiner(", ", Player.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("ip='" + ip + "'")
                .add("name='" + name + "'")
                .add("points=" + points)
                .add("countWins=" + maxWinsCount)
                .add("countLoses=" + maxLosesCount.intValue())
                .toString();
    }
}

