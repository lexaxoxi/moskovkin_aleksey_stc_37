package ru.mosk.game.app;

import ru.mosk.game.dto.StatisticDto;
import ru.mosk.game.repository.*;
import ru.mosk.game.services.GameService;
import ru.mosk.game.services.GameServiceImpl;
import ru.mosk.game.utils.CustomDataSource;

import javax.sql.DataSource;
import java.util.Random;
import java.util.Scanner;

public class Main {

    private static final String JDBC_URL = "jdbc:postgresql://localhost:5432/java_stc37";
    private static final String JDBC_USER = "postgres";
    private static final String JDBC_PASSWORD = "Rubin1987";

    public static void main(String[] args) {
//        PlayersRepository playersRepository = new PlayersRepositoryMapImpl();
//        GamesRepository gamesRepository = new GamesRepositoryImpl();
//        ShotsRepository shotsRepository = new ShotsRepositoryFileImpl("shots_db.txt", "shots_sequence.txt");
//        GameService gameService = new GameServiceImpl(playersRepository, gamesRepository, shotsRepository);

        DataSource dataSource = new CustomDataSource(JDBC_USER, JDBC_PASSWORD, JDBC_URL);
        PlayersRepository playersRepository = new PlayersRepositoryJdbcImpl(dataSource);
        GamesRepository gamesRepository = new GameRepositoryJdbcImpl (dataSource);
        ShotsRepository shotsRepository = new ShotsRepositoryJdbcImpl(dataSource);
        GameService gameService = new GameServiceImpl(playersRepository, gamesRepository, shotsRepository);
        int countGame = 0;
        while (countGame < 3) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Введите имя первого игрока: ");
            String first = scanner.nextLine();
            System.out.println("Введите имя второго игрока: ");
            String second = scanner.nextLine();
            Random random = new Random();

            Long gameId = gameService.startGame("127.0.0.1", "127.0.0.2", first, second);
            String shooter = first;
            String target = second;
            int shotsCount = 0;
            while (shotsCount < 10) {
                System.out.println(shooter + "  делает выстрел в " + target);
                scanner.nextLine();
                int success = random.nextInt(2);
                if (success == 0) {
                    System.out.println(" Успешно!");
                    gameService.shot(gameId, shooter, target);
                } else {
                    System.out.println("Промах!");
                }
                String temp = shooter;
                shooter = target;
                target = temp;
                shotsCount++;
            }
            System.out.println("Итог игры: " + "\n");
            StatisticDto statisticDto = gameService.finishGame(gameId);
            System.out.println(statisticDto);
            countGame++;
            System.out.println();
        }
    }
}
