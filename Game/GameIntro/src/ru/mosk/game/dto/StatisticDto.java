package ru.mosk.game.dto;

import ru.mosk.game.models.Game;
import ru.mosk.game.models.Player;

import java.util.StringJoiner;

public class StatisticDto {
    private Game game;

    public StatisticDto(Game game) {
        this.game = game;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    @Override
    public String toString() {
        Player winner;
        if (game.getPlayerFirstShotsCount() > game.getPlayerSecondShotsCount()) {
            winner = game.getPlayerFirst();
        } else if (game.getPlayerFirstShotsCount() < game.getPlayerSecondShotsCount()){
            winner = game.getPlayerSecond();
        } else {

            return "Игра с ID = " + game.getId() + "\n" + "Игрок 1: " + game.getPlayerFirst().getName() +
                    ", сделал попаданий - " + game.getPlayerFirstShotsCount() + ", набрал очков  - " + game.getPlayerFirst().getPoints() + "\n" +
                    "Игрок 2: " + game.getPlayerSecond().getName() + ", сделал попаданий - "
                    + game.getPlayerSecondShotsCount() + ", набрал очков  - " + game.getPlayerSecond().getPoints() + "\n" +
                    "Ничья" + "\n" + "Игра длилась: " + game.getSecondsGameTimeAmount() + " секунд";
        }

        return "Игра с ID = " + game.getId() + "\n" +
                "Игрок 1: " + game.getPlayerFirst().getName() + ", сделал попаданий - " + game.getPlayerFirstShotsCount() + ", набрал очков - " + game.getPlayerFirst().getPoints() + "\n" +
            "Игрок 2: " + game.getPlayerSecond().getName() + ", сделал попаданий - " + game.getPlayerSecondShotsCount() + ", набрал очков - " + game.getPlayerSecond().getPoints() + "\n" +
            "Победа: " + winner.getName() + "\n" +
            "Игра длилась: " + game.getSecondsGameTimeAmount() + " секунд";
    }
}



