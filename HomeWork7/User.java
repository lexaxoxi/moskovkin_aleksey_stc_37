public class User {
    private String firstName;
    private String lastName;
    private int age;
    private boolean isWorker;

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }

    public boolean isWorker() {
        return isWorker;
    }

    @Override
    public String toString() {
        return "User: " +  firstName + ' ' + lastName +                ", возраст:  " + age +
                ", работает ли   " + isWorker;
    }

    private User(Builder builder) {
        this.firstName = builder.firstName;
        this.lastName = builder.lastName;
        this.age = builder.age;
        this.isWorker = builder.isWorker;
    }
    public static class Builder {
        private String firstName;
        private String lastName;
        private int age;
        private boolean isWorker;


        public Builder firstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public Builder lastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public Builder age(int age) {
            this.age = age;
            return this;
        }

        public Builder isWorker(boolean isWorker) {
            this.isWorker = isWorker;
            return this;
        }

        public User build() {
            return new User(this);
        }
    }
}