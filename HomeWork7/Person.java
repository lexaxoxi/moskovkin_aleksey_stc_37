public class Person {
    public class Example {
        private String value = "";

        public Example append(String another) {
            this.value += another;
            return this;
        }

        public String getValue() {
            return value;
        }
    }

}
