public class Main {
    public static void main(String[] args) {
        User user = new User.Builder()
                .firstName("Marsel")
                .lastName("Sidikov")
                .age(26)
                .isWorker(true)
                .build();
        User user1 = new User.Builder()
                .firstName("Alexey")
                .lastName("Moskovkin")
                .isWorker(true)
                .build();

        System.out.println(user);
        System.out.println(user1);
    }
}
