public class Main {

    public static void main(String[] args) {
        // создал список
        InnoList linkedList = new InnoLinkedList();
        InnoList arraylist = new InnoArrayList();
        for (int i = 0; i < 10; i++) {
            arraylist.add(i);
            linkedList.add(i);
        }

        System.out.println("Исходный список:   " + arraylist);//++
        arraylist.remove(7);
        System.out.println("Удалили заданный элемент из списка:  " + arraylist);
        arraylist.insert(3, 99);//
        System.out.println("Добавили элемент в заданную позицию:  " + arraylist);
        arraylist.addToBegin(3);//переделать, чтоб не выводились 0
        System.out.println("Добавили в начало элемент: " + arraylist);
        arraylist.removeByIndex(4);
        System.out.println("Удалили элемент из заданной позицию списка:  " + arraylist);


        System.out.println("Проверяем список на наличие заданного элемента:  " + arraylist.contains(25));//проверка наличие элемента - boolean

        System.out.println();


        ((InnoLinkedList) linkedList).print();
        System.out.println(linkedList.contains(37));
        linkedList.remove(0);//++
        ((InnoLinkedList) linkedList).print();
        linkedList.addToBegin(4);//++
        ((InnoLinkedList) linkedList).print();
        linkedList.removeByIndex(8);//++
        ((InnoLinkedList) linkedList).print();
        linkedList.insert(2, 18);//++
        ((InnoLinkedList) linkedList).print();

    }
}
