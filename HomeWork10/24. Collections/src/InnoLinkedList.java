/**
 * 11.03.2021
 * 24. Collections
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// Плюсы - неограниченный размер (ограничен VM), быстрое добавление/удаление в начало/конец
// Минусы - медленный доступ по  индексу
public class InnoLinkedList implements InnoList {

    private static class Node {
        int value;
        Node next;
        Node previous;

        Node(int value) {
            this.value = value;
        }

        public void setNext(Node next) {
            this.next = next;
        }
    }

    private Node first;
    private Node last;

    private int count;

    public void print() {
        System.out.print(first.value + " ");
        Node current = first;
        for (int i = 0; i < count - 1; i++) {
            if (current.next == null) {
                break;
            }
            current = current.next;
            System.out.print(current.value + " ");

        }
        System.out.println();
    }


    // get(7)
    @Override
    public int get(int index) {
        if (index >= 0 && index < count) {
            // начинаем с первого элемента
            Node current = first;
            // отсчитываем элементы с начала списка пока не дойдем до элемента с нужной позицией
            for (int i = 0; i < index; i++) {
                // переходим к следующему
                current = current.next; // семь раз сделаю next
            }
            // возвращаем значение
            return current.value;
        } else {
            return -1;
        }
    }

    @Override
    public void insert(int index, int element) {
        // TODO: реализовать++
        if (index >= 0 && index <= count) {
            Node newNode = new Node(element);
            Node current = first;

            if (index == 0) {
                first = newNode;
                first.next = current;
                count++;
                return;
            }
            for (int i = 1; i <= count; i++) {
                if (index == i) {
                    Node next = current.next;
                    current.setNext(newNode);
                    newNode.next = next;
                    count++;
                    break;
                }
                current = current.next;
            }
        } else if (index < 0) {
            System.out.println("Ошибка!!!! отрицательный индекс");
        } else {
            System.out.println("Ошибка!!!  Индекс превысил длину списка чисел" + count);
        }
    }


    @Override
    public void addToBegin(int element) {
        // TODO: реализовать++
        Node newNode = new Node(element);
        Node current = first;

        if (first != null) {
            first = newNode;
            first.next = current;
            count++;

        } else {
            add(element);
        }

    }

    @Override
    public void removeByIndex(int index) {
        // TODO: реализовать++
        if (index >= 0 && index < count) {
            Node current = first;

            if (index == 0) {
                first = current.next;
                count--;
                return;
            }

            for (int i = 1; i < count; i++) {
                if (index == i) {
                    Node next = current.next.next;
                    current.setNext(next);
                    count--;
                    break;
                }
                current = current.next;
            }
        } else if (index < 0) {
            System.out.println("Ошибка!!!! отрицательный индекс");
        } else {
            System.out.println("Ошибка!!!  Индекс превысил длину списка чисел" + count);
        }
    }

//    @Override
//    public void add(int element) {
//        // новый узел для нового элемента
//        Node newNode = new Node(element);
//        // если список пустой
//        if (first == null) {
//            // новый элемент списка и есть самый первый
//            first = newNode;
//        } else {
//            // если элементы в списке уже есть, необходимо добраться до последнего
//            Node current = first;
//            // пока не дошли до узла, после которого ничего нет
//            while (current.next != null) {
//                // переходим к следующему узлу
//                current = current.next;
//            }
//            // дошли по последнего узла
//            // теперь новый узел - самый последний (следующий после предыдущего последнего)
//            current.next = newNode;
//        }
//        count++;
//    }

    @Override
    public void add(int element) {
        Node newNode = new Node(element);
        if (first == null) {
            first = newNode;
        } else {
            // следующий после последнего - новый узел
            last.next = newNode;

        }
        // новый узел - последний
        last = newNode;
        count++;
    }

    @Override
    public void remove(int element) {
        // TODO: реализовать++
        Node current = first;
        Node previous = first;

        for (int i = 0; i < count; i++) {
            if (element == current.value && i == 0) {
                first = current.next;
                count--;
            } else if (element == current.value) {
                previous.setNext(current.next);
                count--;
                break;
            }
            previous = current;
            current = current.next;
        }
    }

    @Override
    public boolean contains(int element) {
        // TODO: реализовать++
        Node current = first;

        for (int i = 0; i < count; i++) {
            if (element == current.value) {
                return true;
            }
            current = current.next;
        }
        return false;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public InnoIterator iterator() {
        return new InnoLinkedListIterator();
    }

    private class InnoLinkedListIterator implements InnoIterator {

        // ссылка на текущий узел итератора
        private Node current;

        InnoLinkedListIterator() {
            this.current = first;
        }

        @Override
        public int next() {
            int nextValue = current.value;
            // сдвигаем указатель на следующий узел
            current = current.next;
            return nextValue;
        }

        @Override
        public boolean hasNext() {
            // если следующего узла нет - не идем дальше
            return current.next != null;
        }
    }
}
