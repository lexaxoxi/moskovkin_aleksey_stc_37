import java.util.Arrays;

public class InnoArrayList implements InnoList {

    private static final int DEFAULT_SIZE = 10;

    private int elements[];

    private int count;

    public InnoArrayList() {
        this.elements = new int[DEFAULT_SIZE];
        this.count = 0;

    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Array is : [");
        for (int i = 0; i < count; i++) {
            sb
                    .append(elements[i])
                    .append(" ");
        }
        sb.append("]");
        return sb.toString();

    }


    @Override
    public int get(int index) {
        if (index >= 0 && index < count) {
            return elements[index];
        } else {
            return -1;
        }
    }

    private int[] copyArray() {
        int[] tempArray = new int[elements.length];
        return tempArray;

    }


    @Override
    public void insert(int index, int element) {

//        //Замена элемента в определенном индексе
//     * @param index куда хотим вставить элемент
//                * @param element сам элемент
        // TODO: реализовать++
        if (count == elements.length) {
            int newElements[] = new int[elements.length + 1];
            // копируем из старого массива все элементы в новый
            for (int i = 0; i < count; i++) {
                newElements[i] = elements[i];
            }
            // устанавливаем ссылку на новый массив
            this.elements = newElements;

        }
        int[] newTempArray = copyArray();

        if (index >= 0 && index <= count) {
            for (int i = 0; i < index; i++) {
                newTempArray[i] = elements[i];
            }
            newTempArray[index] = element;

            for (int i = index; i < count; i++) {
                newTempArray[i + 1] = elements[i];
            }
            elements = newTempArray;
            count++;

        } else if (index > count) {
            index = count;

            for (int i = 0; i < count; i++) {
                newTempArray[i] = elements[i];

            }
            newTempArray[index] = element;
            elements = newTempArray;
            count++;
        } else {
            System.out.println("Ошибка!!!! отрицательный индекс");
        }
    }

    private void resize() {
        // создаем новый массив в полтора раза больший
        int newElements[] = new int[elements.length + elements.length / 2];
        // копируем из старого массива все элементы в новый
        for (int i = 0; i < count; i++) {
            newElements[i] = elements[i];
        }
        // устанавливаем ссылку на новый массив
        this.elements = newElements;
    }

    @Override
    public void addToBegin(int element) {  //добавление элемента в начало
        // TODO: реализовать++
        int[] tempArray = new int[elements.length + 1];
        //Вставляем в нулевую ячейку нужное значение и дозаполняем элементами изначального массива
        //И приравниваем изначальный массив к временному
        for (int i = 1; i <= elements.length; i++) {
            tempArray[0] = element;
            tempArray[i] = elements[i - 1];
        }
        elements = tempArray;
    }

    @Override
    public void removeByIndex(int index) {  // Удаляет элемент в заданной позиции @param index позиция элемента
        // TODO: реализовать++
        if (index >= 0 && index < count) {
            for (int i = 0; i < index; i++) {
                elements[i] = elements[i];

            }

        } else if (index >= count) {
            System.out.println("Ошибка!!!  Индекс превысил длину массива");

        } else if (index < 0) {
            System.out.println("Ошибка!!!! отрицательный индекс");
        }
    }

    @Override
    public void add(int element) {
        // если список переполнен
        if (count == elements.length) {
            resize();
        }

        elements[count++] = element;
    }

    @Override
    public void remove(int element) {  //  * Удаляет элемент из коллекции @param element удаляемый элемент
        // TODO: реализовать++


        int deleteCount = 0;

        for (int i = 0; i < elements.length; i++) {

            if (element == elements[i]) {
                deleteCount++;
                count--;

            } else {
                elements[i - deleteCount] = elements[i];

            }
        }
    }


    @Override
    public boolean contains(int element) {
        /**
         * Проверяет наличие элемента в коллекции
         * @param element искомый элемент
         * @return true, если элемент найден, false в противном случае
         */
        // TODO: реализовать++
        for (int i = 0;
             i < elements.length;
             i++) {
            if (element == elements[i]) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public InnoIterator iterator() {
        // возвращаем новый экземпляр итератора
        return new InnoArrayListIterator();
    }

    // внутренний класс позволяет инкапсулировать логику одного класса внутри класса
    private class InnoArrayListIterator implements InnoIterator {
        // текущая позиция итератора
        private int currentPosition;

        @Override
        public int next() {
            // берем значение под текущей позицией итератора
            int nextValue = elements[currentPosition];
            // увеличиваем позицию итератора
            currentPosition++;
            // возвращаем значение
            return nextValue;
        }

        @Override
        public boolean hasNext() {
            // если текущая позиция не перевалила за общее количество элементов - можно дальше
            return currentPosition < count;
        }
    }
}


