import java.util.Arrays;
		
class Program {	
	public static int binarySearch(int array[], int element,int firstElement, int lastElement) {
		if (lastElement <= firstElement) {
			return -1;
	        }
	    int mid = firstElement + (lastElement - firstElement) / 2;
        if (array[mid] ==element){
        	 return mid;
        } 
        else if (array[mid] < element) {
	        return binarySearch(array, element,mid + 1, lastElement);
	    } else { 
	       	return binarySearch(array, element,firstElement, mid - 1);
	   	}	
	}
	public static void main(String[] args) {   	
	   	int array[] = {6,8,9,12,37,61,81,91,97};
		System.out.println(Arrays.toString(array));					
		System.out.println(binarySearch(array,91,0,array.length-1));
	}
}   				