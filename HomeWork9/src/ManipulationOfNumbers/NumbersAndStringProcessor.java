package ManipulationOfNumbers;

public class NumbersAndStringProcessor {
    // private int number;
    private int arrayOfDigits[];//массив чисел
    private String arrayOfWords[]; //массив строк
    private int quantityOfDigits;  // количество цифр

    public NumbersAndStringProcessor(int numbers[]) {
        this.arrayOfDigits = numbers;
    }

    public int[] process(NumbersProcess process) {
        int[] resultArray = new int[arrayOfDigits.length];
        for (int i = 0; i < arrayOfDigits.length; i++) {
            int number = arrayOfDigits[i];
            int resultNumber = process.process(number);
            resultArray[i] = resultNumber;
        }
        return resultArray;
    }
    public NumbersAndStringProcessor(String words[]) {
        this.arrayOfWords = words;
    }

    public String[] processForWords(StringProcess process) {
        String[] resultArray = new String[arrayOfWords.length];
        for (int i = 0; i < arrayOfWords.length; i++) {
            String words = arrayOfWords[i];
            String resultWords = process.process(words);
            resultArray[i] = resultWords;
        }

        return resultArray;
    }
}
