package ManipulationOfNumbers;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        int[] arrayForNumbers = new int[]{3504, 6549};
        NumbersAndStringProcessor proc = new NumbersAndStringProcessor(arrayForNumbers);

        NumbersProcess reverseProcess = number -> {
            int digit, sum = 0;

            while (number > 0) {
                digit = number % 10;
                number /= 10;
                sum = sum * 10 + digit;
            }
            System.out.println("Перевернутое число: " + sum);
            return sum;

        };
        int[] functionReverseProcess = proc.process(reverseProcess);
        System.out.println("Переносим числа в массив: ");
        System.out.println(Arrays.toString(functionReverseProcess));
        System.out.println("   ");

        NumbersProcess withoutNull = number -> {
            int numberWithoutNull = 0;
            int tempDigitCapacity = 1;
            while (number > 0) {
                int temp = number % 10;
                if (temp != 0) {
                    numberWithoutNull = numberWithoutNull + tempDigitCapacity * temp;
                    tempDigitCapacity = tempDigitCapacity * 10;
                }
                number = number / 10;
            }
            if (numberWithoutNull != 0)
                System.out.println("Число без 0:  " + numberWithoutNull);
            return numberWithoutNull;
        };
        int[] functionWithoutNull = proc.process(withoutNull);
        System.out.println("Переносим числа в массив: ");
        System.out.println(Arrays.toString(functionWithoutNull));
        System.out.println("   ");


        NumbersProcess onlyEvens = number -> {
            int numberWithEvens = 0;
            int newTempForDigitCapacity = 1;
            while (number > 0) {
                if (number % 2 != 0) {
                    number -= 1;
                }
                numberWithEvens = numberWithEvens + ((number % 10) * newTempForDigitCapacity);
                newTempForDigitCapacity = newTempForDigitCapacity * 10;
                number = number / 10;
            }
            System.out.println("Заменил нечетные на четные снизу:  " + numberWithEvens);
            return numberWithEvens;
        };
        int[] functionOnlyEvens = proc.process(onlyEvens);
        System.out.println("Переносим числа в массив: ");
        System.out.println(Arrays.toString(functionOnlyEvens));
        System.out.println("   ");


        String[] arrayForWords = new String[]{"test96ofText", "Text 17"};
        NumbersAndStringProcessor forWords = new NumbersAndStringProcessor(arrayForWords);

        StringProcess reverseOfWords = words -> {
            char symbols[] = words.toCharArray();
            String functionReverseOfWords = "";
            for (int i = symbols.length - 1; i >= 0; i--) {
                functionReverseOfWords = functionReverseOfWords + symbols[i];
            }

            return functionReverseOfWords;

        };
        final String[] strings = forWords.processForWords(reverseOfWords);
        System.out.println("Слова перевернули задом наперёд: ");
        Arrays.stream(strings).forEach(System.out::println);  //вообще первый раз такое вижу ::
        System.out.println("   ");


        StringProcess wordsWithoutNumbers = words -> {
            String functionWordsWithoutNumbers = words.replaceAll("\\d", "");//почитать про replace и replaceall

            System.out.println(functionWordsWithoutNumbers);
            return functionWordsWithoutNumbers;
        };
        System.out.println("Слова без цифр: ");
        final String[] newString = forWords.processForWords(wordsWithoutNumbers);
        System.out.println("   ");


        StringProcess bigWords = words -> {
            String functionBigWords = words.toUpperCase();


            System.out.println(functionBigWords);
            return functionBigWords;
        };
        System.out.println("Все буквы СТРОЧНЫЕ: ");
        final String[] strings2 = forWords.processForWords(bigWords);
        System.out.println("   ");

    }
}







