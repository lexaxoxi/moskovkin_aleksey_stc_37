package ru.inno.game.repository;

import ru.inno.game.models.Shot;



public interface ShotsRepository {
    /**
     * Метод, сохранения выстрела
     *
     * @param shot - выстрел
     */
    void save(Shot shot);
}
