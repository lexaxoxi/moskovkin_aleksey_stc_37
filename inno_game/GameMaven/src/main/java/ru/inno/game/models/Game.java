package ru.inno.game.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;



@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Game {
    private Long id;                                                    //Идентификатор игры
    private LocalDateTime dateTime;                                     //Дата
    private Player playerFirst, playerSecond;                           //Игроки
    private Integer countShotsPlayerFirst, countShotsPlayerSecond;      //Количество выстрелов от каждого игрока
    private Long secondsGameTimeAmount;                                 //Длительность игры
}
