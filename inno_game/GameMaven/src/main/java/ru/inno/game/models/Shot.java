package ru.inno.game.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;



@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Shot {
    private Long id;                    //Идентификатор игры
    private LocalDateTime shotTime;     //Время выстрела
    private Game game;                  //В какой игре
    private Player shooter;             //Стрелок (кто попал)
    private Player target;              //Мишень (в кого попали)
}
