# Описание системы

* `app` - инициализация и запуск приложения.
    `Main` - точка входа для приложения реализуемое на основе списков, файлов и баз данных
    `MainServer` - точка входа клиент-серверного приложения взаимодействующий с базой данных
  
* `dto` - `DataTransferObject`, объекты передачи данных. Используются для вывода или ввода информации, которая не сразу 
  попадает в хранилище, или вообще туда не попадает.
    `Statistic Dto` - информация об игре.
  
* `models` - модели предметной области, это то, что мы хотим хранить и иметь к этому доступ (данные).
    `Game` - игра
    `Player` - пользователь
    `Shot` - выстрел

* `repository` - классы, которые описывают поведение хранилища моделей. Как правило, они представлены интерфейсами 
  `GameRepository`, `PlayersRepository`, `ShotsRepository` чтобы остальные компоненты системы не зависели от принципа
  хранения информации. На основании списков (`GameRepositoryListImpl`, `PlayersRepositoryMapImpl`, `ShotsRepositoryListImpl`), 
  на основании файлов (`PlayersRepositoryFilesImpl`, `ShotsRepositoryFilesImpl`) и на основании базы данных: 
  (`GameRepositoryJdbcImpl`, `PlayersRepositoryJdbcImpl`, `ShotsRepositoryJdbcImpl`).

* `server`  - сервер
  
    `CommandsParser` - парсер команд.
  
    `GameServer` - сервер (класс) - отвечает за подключение игроков по протоколу socket.

* `serivces` - бизнес-логика, проектируются по принципу "Фасад".


