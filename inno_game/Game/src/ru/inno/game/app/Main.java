package ru.inno.game.app;

import ru.inno.game.dto.StatisticDto;
import ru.inno.game.repository.*;
import ru.inno.game.services.GameService;
import ru.inno.game.services.GameServiceImpl;
import ru.inno.game.utils.CustomDataSource;

import javax.sql.DataSource;
import java.util.Random;
import java.util.Scanner;

public class Main {

    private static final String JDBC_URL = "jdbc:postgresql://localhost:5432/Game";
    private static final String JDBC_USER = "postgres";
    private static final String JDBC_PASSWORD = "Rubin1987";

    public static void main(String[] args) {

//        PlayersRepository playersRepository = new PlayersRepositoryMapImpl();
//        GameRepository gameRepository = new GameRepositoryListImpl();
//        ShotsRepository shotsRepository = new ShotsRepositoryListImpl();
//        PlayersRepository playersRepository = new PlayersRepositoryFilesImpl("players_db.txt", "players_sequence.txt");
//        ShotsRepository shotsRepository = new ShotsRepositoryFilesImpl("shots_db.txt", "shots_sequence.txt");

        DataSource dataSource = new CustomDataSource(JDBC_USER, JDBC_PASSWORD, JDBC_URL);
        PlayersRepository playersRepository = new PlayersRepositoryJdbcImpl(dataSource);
        GameRepository gameRepository = new GameRepositoryJdbcImpl(dataSource);
        ShotsRepository shotsRepository = new ShotsRepositoryJdbcImpl(dataSource);
        GameService gameService = new GameServiceImpl(playersRepository, gameRepository, shotsRepository);
        int countGames = 0;
        while (countGames < 3) {
            System.out.println("Начало игры...Загрузка 98%....");
            Scanner scanner = new Scanner(System.in);
            System.out.print("Имя первого игрока: ");
            String fistPlayerName = scanner.nextLine();
            System.out.print("Имя второго игрока: ");
            String secondPlayerName = scanner.nextLine();
            Random random = new Random();

            Long gameId = gameService.startGame("127.0.0.1", "127.0.0.2", fistPlayerName, secondPlayerName);
            String shooter = fistPlayerName;
            String target = secondPlayerName;
            int countShots = 0;
            while (countShots < 10) {
                System.out.println(shooter + " делает выстрел в " + target);
                scanner.nextLine();
                int success = random.nextInt(2);
                if (success == 0) {
                    System.out.println("Попадание");
                    gameService.shot(gameId, shooter, target);
                } else {
                    System.out.println("Промазал!");
                }
                String temp = shooter;
                shooter = target;
                target = temp;
                countShots++;
            }
            System.out.println();
            StatisticDto statisticDto = gameService.finishGame(gameId);
            System.out.println(statisticDto);
            countGames++;
            System.out.println();
        }
    }
}
