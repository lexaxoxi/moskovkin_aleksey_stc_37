package ru.inno.game.client.socket;

import javafx.application.Platform;

import ru.inno.game.client.controllers.MainController;
import ru.inno.game.client.utils.GameUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketException;




public class SocketClient extends Thread {
    private Socket socket;              // канал подключения
    private PrintWriter toServer;       // стрим для отправления сообщений серверу
    private BufferedReader fromServer;  // стрим для получения сообщений от сервера
    private MainController controller;
    private GameUtils gameUtils;
    private String messageFromServer;

    public SocketClient(MainController controller, String host, int port) {
        try {
            // создаем подключение к серверу
            socket = new Socket(host, port);
            // получаем стримы для чтения и записи
            toServer = new PrintWriter(socket.getOutputStream(), true);
            fromServer = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            this.controller = controller;
            this.gameUtils = controller.getGameUtils();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    // ждем сообщений от сервера
    @Override
    public void run() {
        while (true) {
            try {
                // прочитали сообщение с сервера
                messageFromServer = fromServer.readLine();
                if (messageFromServer != null) {
                    switch (messageFromServer) {
                        case "left":
                            //двигаем врага налево
                            gameUtils.goLeft(controller.getEnemy());
                            break;
                        case "right":
                            //двигаем врага направо
                            gameUtils.goRight(controller.getEnemy());
                            break;
                        case "shot":
                            Platform.runLater(() -> gameUtils.createBulletFor(controller.getEnemy(), true));
                            break;
                    }
                }
            } catch (SocketException e) {
                System.out.println("ИГРА ЗАВЕРШЕНА, СОЕДИНЕНИЕ ПРЕРВАНО.");
                break;
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        }
    }

    // отправляем сообщение серверу
    public void sendMessage(String message) {
        toServer.println(message);
    }

    public String getMessageFromServer() {
        return messageFromServer;
    }
}





