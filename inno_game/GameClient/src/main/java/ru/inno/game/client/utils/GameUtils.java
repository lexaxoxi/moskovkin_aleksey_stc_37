package ru.inno.game.client.utils;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.util.Duration;
import ru.inno.game.client.controllers.MainController;
import ru.inno.game.client.socket.SocketClient;

import java.util.List;



public class GameUtils {
    private static final int PLAYER_STEP = 10;
    private static final int DAMAGE = 5;

    private AnchorPane pane;
    private MainController controller;
    private SocketClient client;

    //Движение влево
    public void goLeft(ImageView player) {
        if (player.getX() >= -10) {
            player.setX(player.getX() - PLAYER_STEP);
        }
    }

    //Движение вправо
    public void goRight(ImageView player) {
        if (player.getX() <= 630) {
            player.setX(player.getX() + PLAYER_STEP);
        }
    }

    //создание пули
    public Circle createBulletFor(ImageView player, boolean isEnemy) {
        Circle bullet = new Circle();
        bullet.setRadius(8);
        pane.getChildren().add(bullet);
        bullet.setCenterX(player.getX() + player.getLayoutX() + 40);
        bullet.setCenterY(player.getY() + player.getLayoutY() - 50);
        bullet.setFill(Color.ORANGE);

        int value;

        //выстрел вниз/вверх
        if (isEnemy) {
            value = 3;
        } else {
            value = -3;
        }

        final ImageView target;
        final Label targetHp;

        //определяем кто куда стреляет
        if (!isEnemy) {
            target = controller.getEnemy();
            targetHp = controller.getHpEnemy();
        } else {
            target = controller.getPlayer();
            targetHp = controller.getHpPlayer();
        }

        //анимация для того, чтобы пуля летела во врага
        Timeline timeline = new Timeline(new KeyFrame(Duration.seconds(0.005), animation -> {
            bullet.setCenterY(bullet.getCenterY() + value);
            // если пуля еще видна и произошло пересечение
            if (bullet.isVisible() && isIntersects(bullet, target)) {
                //уменьшаем здоровье
                createDamage(targetHp);
                //скрыть пулю
                bullet.setVisible(false);
                if (!isEnemy) {
                    client.sendMessage("DAMAGE");
                }
            }
        }));

        timeline.setCycleCount(500);
        timeline.play();

        return bullet;
    }

    private boolean isIntersects(Circle bullet, ImageView player) {
        return bullet.getBoundsInParent().intersects(player.getBoundsInParent());
    }

    public void createDamage(Label hpLabel) {
        int hpPlayer = Integer.parseInt(hpLabel.getText());
        if (hpPlayer > 0) {
            hpLabel.setText(String.valueOf(hpPlayer - DAMAGE));
        }

        Label endGame = controller.getFinishGame();
        Label exitGame = controller.getExit();
        if (hpPlayer == 0) {
            client.sendMessage("exit");
            endGame.setVisible(true);
            exitGame.setVisible(true);
            exitGame.setText("Конец битвы");
            String[] message = client.getMessageFromServer().split("#");
            StringBuilder s = new StringBuilder();
            for (String value : message) {
                s.append(value).append("\n");
            }
            endGame.setText(s.toString());
        }
    }

    public void setClient(SocketClient client) {
        this.client = client;
    }

    public void setController(MainController controller) {
        this.controller = controller;
    }

    public void setPane(AnchorPane pane) {
        this.pane = pane;
    }
}