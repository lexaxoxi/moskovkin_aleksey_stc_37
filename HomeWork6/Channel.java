import java.util.Random;

//характеристики канала - название и номер, привязка с программами
public class Channel {

    private String name;
    private Program[] programs;
    private Program currentProgram;

    public Channel(Program[] program, String name) {

        this.programs = program;
        this.name = name;
        Random random = new Random();
        int randomIndex = random.nextInt(program.length);
        currentProgram = programs[randomIndex];
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Program[] getPrograms() {
        return programs;
    }

    public Program getCurrentProgram() {
        return currentProgram;
    }
}