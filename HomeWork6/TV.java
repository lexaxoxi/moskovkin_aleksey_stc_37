public class TV {

    private int numberOfChannel;//поменял название переменной для индекса в массиве

    private Channel[] channels;

    public TV(Channel[] channels) {
        this.channels = channels;
    }

    public void switching(int requiredChannel) { // по сути это метод выбора 0,1,2....n в массиве
        if (requiredChannel < channels.length && requiredChannel > 0) {
            Channel currentChannel = channels[requiredChannel-1];  //из массива каналов кладем в переменую currentChannel
            Program[] programs = currentChannel.getPrograms();//получили список программ определенного канала
            Program currentProgram = currentChannel.getCurrentProgram();
            System.out.println("смотрим канал:  " + currentChannel.getName() + "\n" + "смотрим программу:  " + currentProgram.getName());
            return;
        }

        System.out.println("Такого канала нет, выберите канал от 1 до " +channels.length);

    }

}