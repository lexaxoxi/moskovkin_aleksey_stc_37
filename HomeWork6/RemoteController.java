public class RemoteController {
    private TV tv;


    public RemoteController(TV tv) {
        this.tv = tv;
    }

    public void switchChannel(int requiredChannel) {
        this.tv.switching(requiredChannel);
    }
}
