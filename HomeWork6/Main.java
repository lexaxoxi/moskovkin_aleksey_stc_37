public class Main {

    public static void main(String[] args) {

        Program program1 = new Program("Кинокомиксы Marvel");
        Program program2 = new Program("Кинокомиксы DC");
        Program program3 = new Program("Новинки 2021");
        Program program4 = new Program("Нолан - гений");
        Program program5 = new Program("Симпсоны");
        Program program6 = new Program("Южный парк");
        Program program7 = new Program("Союзмультфильм");
        Program program8 = new Program("Российская Премьер-Лига");
        Program program9 = new Program("Английская Премьер-Лига");
        Program program10 = new Program("Лига Чемпионов 2020-2021");

        Program[] programArr1 = new Program[4];
        programArr1[0] = program1;
        programArr1[1] = program2;
        programArr1[2] = program3;
        programArr1[3] = program4;
        Program[] programArr2 = new Program[3];
        programArr2[0] = program5;
        programArr2[1] = program6;
        programArr2[2] = program7;
        Program[] programArr3 = new Program[3];
        programArr3[0] = program8;
        programArr3[1] = program9;
        programArr3[2] = program10;

        Channel channelOne = new Channel(programArr1, "Фильмы");
        Channel channelTwo = new Channel(programArr2, "Мультики");
        Channel channelThree = new Channel(programArr3, "Спорт");

        Channel[] channelArray = new Channel[3];
        channelArray[0] = channelOne;
        channelArray[1] = channelTwo;
        channelArray[2] = channelThree;

        TV tv = new TV(channelArray);

        RemoteController remoteController = new RemoteController(tv);

        remoteController.switchChannel(-1);

    }
}